/** @format */

function copyCode(codeId) {
  var copyText = document.getElementById(codeId);
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  navigator.clipboard.writeText(copyText.value);
  var button = document.getElementById(`button-${codeId}`);
  button.innerHTML = "copied!";
}

function outCopy(codeId) {
  var button = document.getElementById(`button-${codeId}`);
  button.innerHTML = "copy";
}

function openLang(evt, lang, classTab, classTablinks) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName(classTab);
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName(classTablinks);
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(lang).style.display = "block";
  evt.currentTarget.className += " active";
}

// Table super powers ⚡
$(document).ready(function () {
  $('table').DataTable({
    paging: false,
    order: [],
    dom: 'Bfrtip',
    buttons: ["copy", "excel", "csv"],
    searching: false,
  });
});
