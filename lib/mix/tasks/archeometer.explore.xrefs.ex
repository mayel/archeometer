defmodule Mix.Tasks.Arch.Explore.Xrefs do
  use Mix.Task

  @moduledoc """
  Mix Task to analyze internal dependencies between modules and store them into a Archeometer database.
  """

  @shortdoc "Runs cross reference analysis"

  @impl Mix.Task
  def run(_argv) do
    if Archeometer.Repo.db_ready?() do
      Mix.shell().info("Starting cross reference analysis..")
      Mix.shell().info("Saving cross references...")

      Archeometer.Explore.XRef.compile_xrefs()
      |> Archeometer.Util.DumpStats.save_xrefs()

      Mix.shell().info("Done!")
    else
      Mix.shell().error("Please run static analysis first")
    end
  end
end
