defmodule Mix.Tasks.Arch.Treemap do
  @moduledoc """
  Mix Task to generate a treemap for an application or a set of modules.

  Usage:

      mix arch.treemap [options]

  The following options are accepted:

    * `--metric - Temporarily there is just one metric: size (default)
    * `--namespace - Namespace of the modules considered
    * `--app` - Application name
    * `--db` - Database filename
    * `--out` - Output filename
    * `--skip-tests` - Skips test related modules (default)
  """
  @shortdoc "Generates a Treemap diagram"

  require Logger
  use Mix.Task
  use Archeometer.Repo

  alias Archeometer.Analysis.Treemap
  alias Archeometer.Analysis.Treemap.SVGRender

  @impl Mix.Task
  def run(argv) do
    case get_args(argv) do
      [
        metric: metric,
        app: app,
        namespace: namespace,
        out: out_fname,
        db_name: db_name,
        skip_tests: skip_tests
      ] ->
        case treemap_analysis(String.to_atom(metric), app, namespace, db_name, skip_tests) do
          {:ok, svg} ->
            write(svg, out_fname)
            IO.puts(:stderr, "Diagram ready at: '#{out_fname}'")

          {:error, :wrong_arguments} ->
            IO.puts(:stderr, "Error: wrong arguments in Treemap task")
            IO.puts(:stderr, "No Treemap is generated for #{app}.")

          {:error, :no_data} ->
            IO.puts(:stderr, "No data for #{app}, so no Treemap is generated.")
        end

      _ ->
        print_help()
    end
  end

  defp write(svg, "console") do
    IO.puts(svg)
  end

  defp write(svg, file_name) do
    file_name |> Path.dirname() |> File.mkdir_p()
    File.write(file_name, svg)
  end

  defp get_args(argv) do
    {opts, _args, invalid} =
      OptionParser.parse(
        argv,
        strict: [
          metric: :string,
          app: :string,
          ns: :string,
          out: :string,
          db: :string,
          skip_tests: :boolean
        ]
      )

    case invalid do
      [] ->
        metric = Keyword.get(opts, :metric, "size")
        app = Keyword.get(opts, :app, :none)
        namespace = Keyword.get(opts, :ns, "*")
        out = Keyword.get(opts, :out, "console")
        db_name = Keyword.get(opts, :db, default_db_name())
        skip_tests = Keyword.get(opts, :skip_tests, true)

        [
          metric: metric,
          app: app,
          namespace: namespace,
          out: out,
          db_name: db_name,
          skip_tests: skip_tests
        ]

      _ ->
        {:error, :arguments}
    end
  end

  defp treemap_analysis(metric, app, namespace, db_name, skip_tests) do
    try do
      tree = Treemap.treemap(metric, app, namespace, db_name, skip_tests)

      if tree do
        svg = SVGRender.render(tree)
        {:ok, svg}
      else
        {:error, :no_data}
      end
    rescue
      error ->
        Logger.error(inspect(error))
        {:error, :wrong_arguments}
    end
  end

  defp print_help() do
    IO.puts(:stderr, "Invalid params.")
    IO.puts(:stderr, "Usage: mix arch.treemap [opts] namespace")

    IO.puts(
      :stderr,
      "opts: --metric metric --app app --ns namespace ---db db_file_name  --out fname --skip-tests (default)"
    )
  end
end
