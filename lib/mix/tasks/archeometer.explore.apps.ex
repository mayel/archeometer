defmodule Mix.Tasks.Arch.Explore.Apps do
  use Mix.Task
  alias Archeometer.Repo
  alias Archeometer.Explore.Project
  alias Archeometer.Util.DumpStats

  @moduledoc """
  Mix Task to collect application names and modules application id and store
  them into a Archeometer database.
  """

  @shortdoc "Dumps modules application id of current project into Archeometer DB"

  defp missing_app_modules(),
    do: Repo.all("SELECT m.name, m.path FROM modules m WHERE m.app_id IS NULL;")

  defp guess_app_and_format(%Repo.Result{rows: rows}) do
    rows
    |> Enum.map(fn [name, path] ->
      case Project.guess_app_from_path(path) do
        {:ok, app} -> [{name, app}]
        {:error, _} -> []
      end
    end)
    |> Enum.concat()
    |> Enum.group_by(&elem(&1, 1), &elem(&1, 0))
  end

  @impl Mix.Task
  def run(_argv) do
    if Archeometer.Repo.db_ready?() do
      Mix.shell().info("Starting application classification analysis...")

      Mix.Task.run("compile")

      apps = Project.apps()
      Mix.shell().info("Saving #{Enum.count(apps)} apps...\n")

      Archeometer.Util.DumpStats.save_apps(apps)

      Mix.shell().info("Saving app modules...\n")

      Project.local_modules_by_app()
      |> Enum.map(fn {app, modules} -> %{app: app, modules: modules} end)
      |> DumpStats.add_modules_app_id()

      missing_app_modules()
      |> guess_app_and_format()
      |> Enum.map(fn {app, modules} -> %{app: app, modules: modules} end)
      |> DumpStats.add_modules_app_id()

      Mix.shell().info("Done!\n")
    else
      Mix.shell().error("Please run static analysis first")
    end
  end
end
