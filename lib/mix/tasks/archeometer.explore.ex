defmodule Mix.Tasks.Arch.Explore do
  use Mix.Task

  @moduledoc """
  Mix task to gather information about the project and store it into the database.
  It currently gathers information about:

  - Inventory of applications, modules, functions and macros.
  - Metrics related to them such as: size and complexity.
  - Information about test coverage.
  - Internal dependencies between modules.

  """

  @shortdoc "Explore current project and stores info into Archeometer DB"

  @impl Mix.Task
  def run(_argv) do
    if Mix.Task.run("arch.explore.static") == 0 do
      Mix.Task.run("arch.explore.xrefs")
      Mix.Task.run("arch.explore.apps")
      # Coverage needs to be called as a command in order to set the mix testing environment
      Mix.Shell.cmd("mix arch.explore.coverage", [env: %{"MIX_ENV" => "test"}], &IO.puts/1)
    else
      Mix.shell().error("Could not run static exploration")
    end
  end
end
