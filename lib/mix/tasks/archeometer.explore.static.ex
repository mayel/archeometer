defmodule Mix.Tasks.Arch.Explore.Static do
  use Mix.Task

  @moduledoc """
  Mix Task to run code AST exploration of modules, functions and macros
  and store its findings into a Archeometer database.
  """

  @shortdoc "Runs static code exploration"

  @impl Mix.Task
  def run(_argv) do
    Credo.Application.start(nil, nil)

    %Credo.Execution{argv: []}
    |> Credo.Execution.ExecutionTiming.start_server()
    |> Credo.Execution.ExecutionConfigFiles.start_server()
    |> Credo.Execution.ExecutionSourceFiles.start_server()
    |> Credo.Execution.Task.AppendDefaultConfig.call([])
    |> Credo.Execution.Task.ParseOptions.call([])
    |> Credo.ConfigBuilder.parse()
    |> Credo.CLI.Task.LoadAndValidateSourceFiles.call()
    |> Archeometer.Explore.Credo.SaveStatsTask.call()
    |> Credo.Execution.get_exit_status()
  end
end
