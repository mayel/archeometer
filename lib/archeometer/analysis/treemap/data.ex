defmodule Archeometer.Analysis.Treemap.Data do
  @moduledoc false

  use Archeometer.Repo
  alias Archeometer.Explore.Project

  def get_data(:size, app, namespace, db_name, skip_tests) do
    {:ok, conn} = DB.open(db_name)

    modules = get_modules_and_lcount(conn, app, namespace, skip_tests)

    DB.close(conn)

    modules
  end

  defp get_modules_and_lcount(conn, app, ns, skip_tests) do
    modules = get_modules(conn, app, ns)

    modules =
      if skip_tests do
        Enum.filter(modules, &(is_test?(&1) == false))
      else
        modules
      end

    Enum.map(modules, fn [id, name, num_lines, _path] -> {id, name, num_lines} end)
  end

  defp get_modules(conn, :none, "*") do
    execute_query(conn, "SELECT id, name, num_lines, path FROM modules", [])
  end

  defp get_modules(conn, :none, ns) do
    exact_match =
      conn
      |> execute_query("SELECT id, name, num_lines, path FROM modules WHERE name = ?1", [ns])

    ns_matches =
      conn
      |> execute_query("SELECT id, name, num_lines, path FROM modules WHERE name like ?1", [
        ns <> ".%"
      ])

    exact_match ++ ns_matches
  end

  defp get_modules(conn, app, "*") do
    query = """
    SELECT m.id, m.name, m.num_lines, m.path
    FROM modules m
    INNER JOIN apps a
    ON m.app_id = a.id
    WHERE a.name = ?1
    """

    execute_query(conn, query, [app])
  end

  defp get_modules(conn, app, ns) do
    exact_query = """
    SELECT m.id, m.name, m.num_lines, m.path
    FROM modules m
    INNER JOIN apps a
    ON m.app_id = a.id
    WHERE m.name = ?1
    AND a.name = ?2
    """

    exact_match =
      conn
      |> execute_query(exact_query, [ns, app])

    ns_query = """
    SELECT m.id, m.name, m.num_lines, m.path
    FROM modules m
    INNER JOIN apps a
    ON m.app_id = a.id
    WHERE m.name like ?1
    AND a.name = ?2
    """

    ns_matches =
      conn
      |> execute_query(ns_query, [ns <> ".%", app])

    exact_match ++ ns_matches
  end

  defp is_test?([_id, _name, _num_kines, path]) do
    if Project.umbrella_or_multirepo?() or is_archeometer_test?() do
      Regex.match?(~r{[apps|deps|forks]/.+/test/}, path)
    else
      String.starts_with?(path, "test/")
    end
  end

  defp is_archeometer_test?() do
    Mix.env() == :test and Keyword.get(Mix.Project.config(), :app) == :archeometer
  end
end
