defmodule Archeometer.Analysis.Treemap do
  @moduledoc """
  Functions for generating different TreeMaps.
  """

  defmodule Node do
    @moduledoc false

    defstruct name: nil, nodes: [], kind: :group, val: 0, pct: 0
  end

  def treemap(:size, app, namespace, db_name, skip_tests) do
    modules = __MODULE__.Data.get_data(:size, app, namespace, db_name, skip_tests)

    if is_list(modules) and length(modules) > 0 do
      all_groups = all_groups(modules)

      all_groups
      |> get_roots()
      |> init_tree()
      |> add_groups(all_groups)
      |> add_modules(modules)
      |> aggregate_metric()
      |> distribute_pct()
    end
  end

  defp aggregate_metric(%Node{kind: :group, nodes: nodes} = t) do
    new_nodes = Enum.map(nodes, &aggregate_metric/1)
    new_val = Enum.reduce(new_nodes, 0, fn node, acc -> node.val + acc end)
    %{t | nodes: new_nodes, val: new_val}
  end

  defp aggregate_metric(%Node{kind: :leaf, val: _val} = t), do: t

  defp distribute_pct(%Node{kind: :group, pct: pct, val: val, nodes: nodes} = t) do
    new_nodes =
      Enum.map(nodes, fn node ->
        new_node = %{node | pct: node.val * pct / val}
        distribute_pct(new_node)
      end)

    %{t | nodes: new_nodes}
  end

  defp distribute_pct(%Node{} = t), do: t

  def parents(module) do
    case String.split(module, ".") |> Enum.reverse() do
      [_] ->
        []

      [_ | tail] ->
        groups(tail)
    end
  end

  defp groups([x]), do: [x]
  defp groups([_h | t] = l), do: [l |> Enum.reverse() |> Enum.join(".") | groups(t)]

  defp all_groups(modules) do
    modules
    |> Enum.flat_map(fn {_id, name, _loc} -> parents(name) end)
    |> Enum.uniq()
    |> Enum.sort()
    |> List.delete([])
  end

  defp get_roots(groups) do
    Enum.filter(groups, fn g -> String.split(g, ".") |> length() == 1 end)
  end

  defp init_tree([name]), do: %Node{name: name, nodes: [], pct: 100}

  defp init_tree(names) do
    nodes = Enum.map(names, fn name -> %Node{name: name, nodes: []} end)
    %Node{name: "*", nodes: nodes, pct: 100}
  end

  def add_groups(%Node{} = t, []), do: t

  def add_groups(%Node{name: root_name, nodes: nodes} = t, [group_name | others]) do
    case {subgroup?(root_name, group_name), find_match(nodes, group_name)} do
      {false, _match} ->
        add_groups(t, others)

      {true, nil} ->
        case find_same(nodes, group_name) do
          nil ->
            new_nodes =
              [%Node{name: group_name} | nodes]
              |> Enum.sort_by(fn n -> n.name end)

            new_t = %{t | nodes: new_nodes}
            add_groups(new_t, others)

          _same ->
            add_groups(t, others)
        end

      {true, match} ->
        new_nodes =
          [add_groups(match, [group_name]) | List.delete(nodes, match)]
          |> Enum.sort_by(fn n -> n.name end)

        new_t = %{t | nodes: new_nodes}
        add_groups(new_t, others)
    end
  end

  defp subgroup?("*", _group), do: true

  defp subgroup?(root_name, group) do
    String.starts_with?(group, root_name <> ".")
  end

  defp find_match(nodes, name) do
    Enum.find(nodes, fn node -> String.starts_with?(name, node.name <> ".") end)
  end

  defp find_same(nodes, name) do
    Enum.find(nodes, fn node -> name == node.name end)
  end

  def add_modules(%Node{} = t, []), do: t

  def add_modules(
        %Node{name: node_name, nodes: nodes} = t,
        [{_id, module, val} = curr | others]
      ) do
    new_t =
      case {same_name?(node_name, module), direct_child?(module, node_name)} do
        {true, _} ->
          new_nodes =
            [leaf(module, val) | nodes]
            |> Enum.sort_by(fn n -> n.name end)

          %{t | nodes: new_nodes}

        {false, true} ->
          new_nodes =
            case find_same(nodes, module) do
              nil ->
                [leaf(module, val) | nodes]
                |> Enum.sort_by(fn n -> n.name end)

              match ->
                [add_modules(match, [curr]) | List.delete(nodes, match)]
                |> Enum.sort_by(fn n -> n.name end)
            end

          %{t | nodes: new_nodes}

        {false, false} ->
          new_nodes = Enum.map(nodes, fn node -> add_modules(node, [curr]) end)
          %{t | nodes: new_nodes}
      end

    add_modules(new_t, others)
  end

  defp leaf(name, val) do
    %Node{name: name, kind: :leaf, val: val}
  end

  defp direct_child?(module, root_name) do
    String.split(module, ".") |> Enum.drop(-1) |> Enum.join(".") == root_name
  end

  defp same_name?(module, node_name) do
    module == node_name
  end
end
