defmodule Archeometer.Analysis.Apps.Xref do
  @moduledoc """
  Functions for generating a dependency graph from applicatios within an umbrella application.

  Accepted output formats are "dot" (graphviz), "png" and "mermaid".
  """

  alias Archeometer.Graphs.Graphviz
  alias Archeometer.Graphs.Mermaid

  @supported_formats ["png", "dot", "mermaid", "svg"]

  @doc """
  Creates a dependency graph between the applications of the current project.

  ## Parameters

  - `format` can be one of "dot" (graphviz), "png", or "mermaid".

  ## Returns

  - The binary representing the graph, if the operation was completed successfully.
  - `{:error, reason}` if not.

  """
  def gen_graph(format) when format in @supported_formats do
    do_gen_graph(format, Mix.Project.umbrella?() || Mix.Project.config()[:multirepo_deps])
  end

  def gen_graph(_format) do
    {:error, :unsupported_format}
  end

  defp do_gen_graph(_format, false) do
    {:error, :not_umbrella_project}
  end

  defp do_gen_graph(format, true) do
    Mix.Project.apps_paths()
    |> Enum.map(fn app -> xrefs(app, &in_umbrella?/1) end)
    |> Enum.into(%{})
    |> render(format)
  end

  defp do_gen_graph(format, multirepo_deps) when is_list(multirepo_deps) do
    multirepo_deps
    |> Enum.map(&multirepo_xrefs(&1, Mix.Project.config()[:in_multirepo_fn]))
    |> Enum.into(%{})
    |> render(format)
  end

  defp xrefs({app_id, app_rel_path}, include_fn) when is_binary(app_rel_path) do
    app_path = Path.join(File.cwd!(), app_rel_path)

    xrefs =
      Mix.Project.in_project(app_id, app_path, &refs/1)
      |> Enum.filter(fn dep -> include_fn.(dep) end)
      |> Enum.map(fn
        {dep_id, _} -> dep_id
        {dep_id, _, _} -> dep_id
      end)

    {app_id, xrefs}
  end

  defp multirepo_xrefs({app_id, dep_meta}, include_fn) when is_list(dep_meta) do
    path = Keyword.get(dep_meta, :path) || "deps/#{app_id}"
    xrefs({app_id, path}, include_fn)
  end

  defp multirepo_xrefs({app_id, _version, dep_meta}, include_fn) when is_list(dep_meta) do
    multirepo_xrefs({app_id, dep_meta}, include_fn)
  end

  defp multirepo_xrefs({app_id, _version}, include_fn) do
    multirepo_xrefs({app_id, []}, include_fn)
  end

  defp refs(_module) do
    Mix.Project.config()
    |> Keyword.get(:deps)
  end

  defp in_umbrella?(dep_config) do
    case dep_config do
      {_, _, _} ->
        false

      {_dep_id, cfg} ->
        Keyword.keyword?(cfg) and Keyword.get(cfg, :in_umbrella, false)
    end
  end

  defp render(refs, "dot") do
    Graphviz.render_dot(refs)
  end

  defp render(refs, format) when format in ["png", "svg"] do
    Graphviz.render_image(refs, format)
  end

  defp render(refs, "mermaid") do
    Mermaid.render(refs)
  end
end
