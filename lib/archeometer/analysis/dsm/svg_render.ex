defmodule Archeometer.Analysis.DSM.SVGRender do
  @moduledoc """
  Functions to render an `Archeometer.Analysis.DSM` struct into a
  [svg](https://www.w3schools.com/graphics/svg_intro.asp) image.
  """

  alias Archeometer.Analysis.DSM

  @template "svg/dsm_svg.eex"

  @doc """
  Renders a `DSM` struct into a `svg`.

  Returns the string representing the generated `svg` image.

  The resulting `svg` can be written to a file or directly embedded into an `HTML`.

  ## Parameters

  - `dsm`. The `DSM` to be rendered.
  - `mod_names`. A map from module ids to their corresponding names.

  """
  def render(%DSM{} = dsm, mod_names) do
    group_corners = upper_right_group_corners(dsm)

    @template
    |> Archeometer.Util.PathHelper.template_path()
    |> EEx.eval_file(mtx: dsm, mod_names: mod_names, group_corners: group_corners)
    |> String.replace(~r/\n\s+\n/, "\n")
  end

  defp upper_right_group_corners(dsm) do
    coord_mtx = to_coord_mtx(dsm)
    mtx_size = length(dsm.nodes)

    coord_mtx
    |> Enum.filter(&(above_diagonal?(&1) and upper_right_empty?(&1, coord_mtx, mtx_size)))
    |> Enum.map(fn {{r, c}, _v} -> {r, c} end)
  end

  defp above_diagonal?({{row, col}, _v}) do
    col > row
  end

  defp upper_right_empty?({{row, col}, _v}, coord_mtx, mtx_size) do
    upper_right_positions =
      for r <- 0..row,
          c <- col..(mtx_size - 1),
          not (r == row and c == col),
          do: {r, c}

    Enum.all?(upper_right_positions, fn {r, c} -> Map.get(coord_mtx, {r, c}) == nil end)
  end

  defp to_coord_mtx(dsm) do
    mod_indexes = Enum.with_index(dsm.nodes) |> Enum.into(%{})

    dsm.edges
    |> Enum.map(fn {{callee, caller}, val} ->
      row = Map.get(mod_indexes, callee)
      col = Map.get(mod_indexes, caller)
      {{row, col}, val}
    end)
    |> Enum.into(%{})
  end
end
