defmodule Archeometer.Analysis.DSM.Data do
  @moduledoc false

  use Archeometer.Repo

  def get_data(app, namespace, db_name, skip_tests) do
    {:ok, conn} = DB.open(db_name)

    module_names = get_modules(conn, app, namespace, skip_tests)
    xrefs = get_xrefs(conn, module_names)

    DB.close(conn)

    {module_names, xrefs}
  end

  defp get_modules(conn, app, ns, skip_tests) do
    modules = get_modules(conn, app, ns)

    modules =
      if skip_tests do
        Enum.filter(modules, &(is_test?(&1) == false))
      else
        modules
      end

    modules
    |> Enum.map(fn [id, name, _path] -> {id, name} end)
    |> Enum.into(%{})
  end

  defp get_modules(conn, :none, "*") do
    execute_query(conn, "SELECT id, name, path FROM modules", [])
  end

  defp get_modules(conn, :none, ns) do
    exact_match =
      conn
      |> execute_query("SELECT id, name, path FROM modules WHERE name = ?1", [ns])

    ns_matches =
      conn
      |> execute_query("SELECT id, name, path FROM modules WHERE name like ?1", [ns <> ".%"])

    exact_match ++ ns_matches
  end

  defp get_modules(conn, app, "*") do
    query = """
    SELECT m.id, m.name, m.path
    FROM modules m
    INNER JOIN apps a
    ON m.app_id = a.id
    WHERE a.name = ?1
    """

    execute_query(conn, query, [app])
  end

  defp get_modules(conn, app, ns) do
    exact_query = """
    SELECT m.id, m.name, m.path
    FROM modules m
    INNER JOIN apps a
    ON m.app_id = a.id
    WHERE m.name = ?1
    AND a.name = ?2
    """

    exact_match =
      conn
      |> execute_query(exact_query, [ns, app])

    ns_query = """
    SELECT m.id, m.name, m.path
    FROM modules m
    INNER JOIN apps a
    ON m.app_id = a.id
    WHERE m.name like ?1
    AND a.name = ?2
    """

    ns_matches =
      conn
      |> execute_query(ns_query, [ns <> ".%", app])

    exact_match ++ ns_matches
  end

  defp is_test?([_id, _name, path]) do
    if Mix.Project.umbrella?() or is_archeometer_test?() do
      Regex.match?(~r{apps/.+/test/}, path)
    else
      String.starts_with?(path, "test/")
    end
  end

  defp is_archeometer_test?() do
    Mix.env() == :test and Keyword.get(Mix.Project.config(), :app) == :archeometer
  end

  defp get_xrefs(conn, modules) do
    modules
    |> Map.keys()
    |> Enum.map(&callees(&1, conn, modules))
    |> Enum.into(%{})
  end

  defp callees(id, conn, modules_map) do
    others = Map.keys(modules_map) -- [id]

    callees =
      conn
      |> execute_query("SELECT callee_id FROM xrefs WHERE caller_id = ?1", [id])
      |> List.flatten()
      |> Enum.uniq()
      |> Enum.filter(fn callee_id -> Enum.member?(others, callee_id) end)

    {id, callees}
  end
end
