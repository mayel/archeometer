defmodule Archeometer.Analysis.Xref do
  @moduledoc """
  Functions for generating a dependency graph from a list of given modules.

  Accepted output formats are "dot" (graphviz), "png" and "mermaid".
  """

  use Archeometer.Repo
  alias Archeometer.Graphs.Graphviz
  alias Archeometer.Graphs.Mermaid

  @supported_formats ["png", "dot", "mermaid", "svg"]

  @doc """
  Creates a dependency graph between the modules given as parameters

  ## Parameters

  - `modules` is a list of module names, e.g. `[Foo.Bar, Foo.Rex, Foo.Zorg]`
  - `format` can be one of "dot" (graphviz), "png", or "mermaid".
  - `db_name` is the filename of the DB to be used. If not given uses default DB.

  ## Returns

  - The binary representing the graph, if the operation was completed successfully.
  - `{:error, reason}` if not.

  """
  def gen_graph(modules, format, db_name \\ default_db_name())

  def gen_graph(modules, _, _) when not is_list(modules) or modules == [],
    do: {:error, :no_modules}

  def gen_graph(modules, format, db_name) when format in @supported_formats do
    try do
      do_gen_graph(modules, format, db_name)
    rescue
      e in RuntimeError -> {:error, e.message}
      e in MatchError -> e.term
    end
  end

  def gen_graph(_modules, _format, _db_name) do
    {:error, :unsupported_format}
  end

  defp do_gen_graph(modules, format, db_name) do
    modules
    |> xrefs(db_name)
    |> render(format)
  end

  defp xrefs(modules, db_name) do
    {:ok, conn} = DB.open(db_name)

    module_ids = module_ids(modules, conn)
    modules_map = Enum.zip(module_ids, modules) |> Enum.into(%{})

    xrefs =
      module_ids
      |> Enum.map(&callees(&1, conn, modules_map))
      |> Enum.into(%{})

    DB.close(conn)
    xrefs
  end

  defp module_ids(modules, conn) do
    Enum.map(modules, &module_id(&1, conn))
  end

  defp module_id(m, conn) do
    case execute_query(conn, "SELECT id FROM modules WHERE name = ?1", [m]) do
      [[id]] -> id
      [] -> raise RuntimeError, message: "unknown module '#{m}'"
    end
  end

  defp callees(id, conn, modules_map) do
    others = Map.keys(modules_map) -- [id]

    callees =
      conn
      |> execute_query("SELECT callee_id FROM xrefs WHERE caller_id = ?1", [id])
      |> List.flatten()
      |> Enum.uniq()
      |> Enum.filter(fn callee_id -> Enum.member?(others, callee_id) end)
      |> Enum.map(fn callee_id -> Map.get(modules_map, callee_id) end)

    {Map.get(modules_map, id), callees}
  end

  defp render(refs, "dot") do
    Graphviz.render_dot(refs)
  end

  defp render(refs, format) when format in ["png", "svg"] do
    Graphviz.render_image(refs, format)
  end

  defp render(refs, "mermaid") do
    Mermaid.render(refs)
  end
end
