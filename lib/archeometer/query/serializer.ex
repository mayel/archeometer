defmodule Archeometer.Query.Serializer do
  @moduledoc false

  alias Archeometer.Query.{Term, Symbol, JoinExpr}
  require EEx

  @doc ~S"""
  Takes a `Archeometer.Query` and returns a string containing an equivalent SQL
  query

  ## Examples
  - `like` operator with named columns

          iex> import Archeometer.Query
          iex> q = from m in Archeometer.Schema.Function,
          ...> where: m.num_args > 3,
          ...> where: like(m.name, "Kamaji.Web.%"),
          ...> select: [name: m.name, arity: m.num_args]
          iex> {:ok, {query_str, _binds}} = Archeometer.Query.Serializer.to_sql(q)
          iex> query_str
          "SELECT u0.name,u0.num_args FROM functions u0 WHERE (u0.num_args > 3) AND (u0.name LIKE 'Kamaji.Web.%\') ; "

  - A more complex query

          iex> import Archeometer.Query
          iex> q = from m in Archeometer.Schema.Module,
          ...> select: [m.name, sum(m.functions.cc)],
          ...> where: m.num_lines > 10 and m.coverage < 0.9,
          ...> order_by: [desc: sum(m.functions.cc), asc: avg(m.num_lines)],
          ...> limit: 10
          iex> {:ok, {query_str, _binds}} = Archeometer.Query.Serializer.to_sql(q)
          iex> query_str
          "SELECT u0.name,sum(u1.cc) FROM modules u0 INNER JOIN functions u1 ON u1.module_id = u0.id WHERE ((u0.num_lines > 10) AND (u0.coverage < 0.9)) ORDER BY sum(u1.cc) DESC,avg(u0.num_lines) ASC LIMIT 10 ; "

  """
  def to_sql(query)

  def to_sql({:ok, %{bindings: binds}} = query) do
    query
    |> serialize_query()
    |> ok_do(&embed_query/1)
    |> ok_do(&String.replace(&1, ~r{\n+}, "\n"))
    |> ok_do(&String.replace(&1, ~r{\n}, " "))
    |> ok_do(fn embedded_query -> {embedded_query, binds} end)
  end

  def to_sql({:error, _} = error), do: error

  @sql_select_template """
  SELECT
  <%= if query.distinct, do: "DISTINCT"%>
  <%= format_grouping(query.select) %>

  FROM <%= query.source.module %> <%= query.source.alias %>

  <%= for %{source: s, dest: d} <- query.tables do %>
  INNER JOIN <%= d.module %> <%= d.alias %>
  ON <%= d.alias%>.<%= d.key%> = <%= s.alias%>.<%= s.key %>
  <% end %>

  <%= unless Enum.empty?(query.where) do %>
  WHERE <%= Enum.intersperse(query.where, " AND ")%>
  <% end %>

  <%= unless Enum.empty?(query.group_by) do %>
  GROUP BY <%= format_grouping(query.group_by)%>
  <% end %>

  <%= unless Enum.empty?(query.having) do %>
  HAVING <%= Enum.intersperse(query.having, " AND ")%>
  <% end %>

  <%= unless Enum.empty?(query.order_by) do %>
  ORDER BY <%= format_order(query.order_by)%>
  <% end %>

  <%= if not is_nil(query.limit) do %>
  LIMIT <%= query.limit %>
  <% end %>
  ;
  """

  EEx.function_from_string(:defp, :embed_query, @sql_select_template, [:query])

  defp format_grouping(expr_list),
    do: format_nested_list_pairs(expr_list, &elem(&1, 1))

  defp format_order(expr_list),
    do:
      format_nested_list_pairs(expr_list, fn {key, val} ->
        [val, " ", order_keyword(key)]
      end)

  defp format_nested_list_pairs(list, fun) do
    list
    |> Enum.concat()
    |> Enum.map(fn val ->
      case val do
        {key, v} -> fun.({key, v})
        _ -> val
      end
    end)
    |> Enum.intersperse(",")
  end

  defp order_keyword(:asc), do: "ASC"
  defp order_keyword(:desc), do: "DESC"

  @serializable_fields [:select, :where, :order_by, :group_by, :having]

  @doc """
  Transform each field of the query struct into either io_data or a map where
  the elements are io_data.

  Similar to io_data, but with maps instead of lists.
  """
  def serialize_query(query)

  def serialize_query({:ok, _} = query) do
    Enum.reduce(@serializable_fields, query, &serialize_section(&2, &1))
    |> ok_do(&serialize_source/1)
    |> ok_do(&serialize_joins/1)
  end

  def serialize_query({:error, _} = error), do: error

  defp serialize_source(query),
    do: %{query | source: JoinExpr.Table.serialize(query.source)}

  defp serialize_joins(query) do
    Map.put(
      query,
      :tables,
      for {_name, %JoinExpr{} = join} <- query.tables do
        JoinExpr.serialize(join)
      end
    )
  end

  defp serialize_section({:ok, query}, field) do
    serialize_expr(Map.get(query, field), query.tables)
    |> ok_do(&Map.put(query, field, &1))
  end

  defp serialize_expr(expr_list, table_aliases) when is_list(expr_list) do
    expr_list
    |> Enum.reduce({:ok, []}, &serialize_container_arg(&1, &2, table_aliases))
    |> ok_do(&Enum.reverse/1)
  end

  defp serialize_expr(expr, alias_tables) do
    with {:ok, flat} <- Term.flatten_lookups(expr),
         {:ok, aliased} <- Symbol.replace_aliases(flat, alias_tables) do
      Term.to_iodata(aliased)
    end
  end

  defp serialize_container_arg({key, val}, {:ok, prevs}, table_aliases) do
    serialize_expr(val, table_aliases)
    |> ok_do(fn ser_arg -> [{key, ser_arg} | prevs] end)
  end

  defp serialize_container_arg(arg, {:ok, prevs}, table_aliases) do
    serialize_expr(arg, table_aliases)
    |> ok_do(fn ser_arg -> [ser_arg | prevs] end)
  end

  defp ok_do({:ok, elem}, fun), do: {:ok, fun.(elem)}
  defp ok_do({:error, _} = error, _fun), do: error
end
