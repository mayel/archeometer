defmodule Archeometer.Reports.Render.Html do
  @moduledoc """
  Renders a page into HTML.
  """
  require Slime

  Slime.function_from_file(
    :def,
    :render,
    "lib/templates/report/base_report.slime",
    [
      :app_data,
      :page_names
    ]
  )
end
