defmodule Archeometer.Reports.Page do
  @moduledoc """
  Represents a Page of the report.
  Each page is associated with an application.
  """

  use Archeometer.Repo
  alias Archeometer.Reports.Section

  defstruct [:id, sections: []]

  defmodule Definition do
    @moduledoc """
    Represents the definition of a Page.
    """

    defstruct [:id, sections: []]
  end

  def process(%__MODULE__.Definition{} = pdef, bindings, db_name \\ default_db_name()) do
    sections = Enum.map(pdef.sections, &Section.process(&1, bindings, db_name))
    res = struct(__MODULE__, Map.from_struct(pdef))
    %{res | sections: sections}
  end
end
