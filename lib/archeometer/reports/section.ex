defmodule Archeometer.Reports.Section do
  @moduledoc """
  Represents a Section, which is part of a Page.
  """

  use Archeometer.Repo
  alias Archeometer.Reports.Fragment

  defstruct [:id, :desc, fragments: []]

  defmodule Definition do
    @moduledoc """
    Represents the definition of a Section.
    """

    defstruct [:id, :desc, fragments: []]
  end

  def process(%__MODULE__.Definition{} = sdef, bindings, db_name \\ default_db_name()) do
    {fragments, _} =
      Enum.reduce(sdef.fragments, {[], bindings}, fn fdef, acc ->
        process_fragment(fdef, acc, db_name)
      end)

    res = struct(__MODULE__, Map.from_struct(sdef))
    %{res | fragments: fragments}
  end

  defp process_fragment(
         %Fragment.Definition{result_type: :table} = fdef,
         {results, bindings},
         db_name
       ) do
    fragment = Fragment.process(fdef, bindings, db_name)
    new_bindings = new_bindings(bindings, fdef.result_name, fragment.result)
    {results ++ [fragment], new_bindings}
  end

  defp process_fragment(
         %Fragment.Definition{} = fdef,
         {results, bindings},
         db_name
       ) do
    fragment = Fragment.process(fdef, bindings, db_name)
    {results ++ [fragment], bindings}
  end

  defp new_bindings(bindings, :none, _value) do
    bindings
  end

  defp new_bindings(bindings, name, value) do
    [{name, to_binding_val(value)} | bindings]
  end

  defp to_binding_val(table) when is_map(table) do
    table.values |> Enum.join(" ")
  end
end
