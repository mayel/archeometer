defmodule Archeometer.Util.PathHelper do
  @moduledoc false

  def template_path(template) do
    Path.expand(template, template_path())
  end

  def template_path() do
    case Mix.Project.get() do
      # When archeometer is the current project
      Archeometer.MixProject ->
        "./lib/templates/"

      _ ->
        case archeometer_config() do
          # When archeometer is a local dep
          {:archeometer, path: local_path} ->
            Path.expand("lib/templates", local_path)

          _ ->
            # When archeometer is a remote dep (downloaded to 'deps_path')
            Path.expand("archeometer/lib/templates/", Mix.Project.deps_path())
        end
    end
  end

  defp archeometer_config do
    Mix.Project.config()
    |> Keyword.get(:deps)
    |> Enum.find(fn dep -> elem(dep, 0) == :archeometer end)
  end
end
