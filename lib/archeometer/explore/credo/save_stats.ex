defmodule Archeometer.Explore.Credo.SaveStatsTask do
  @moduledoc """
  Credo Task to collect and save metrics from an static analysis of an Elixir project..
  """

  use Credo.Execution.Task
  alias Credo.CLI.Output.UI
  alias Archeometer.Util.DumpStats
  alias Archeometer.Explore.Static

  def call(exec) do
    sources = Execution.get_source_files(exec)
    # IO.inspect(sources, label: "sources")

    mods = Static.module_stats_for(sources)

    funcs = Static.def_stats_for(sources, [:def, :defp])

    macros = Static.def_stats_for(sources, [:defmacro, :defmacrop])

    UI.puts("\nAnalysis done!\n")

    UI.puts("Saving modules...")
    DumpStats.save_modules(mods)

    UI.puts("Saving functions...")
    DumpStats.save_defs(funcs, "functions")

    UI.puts("Saving macros...")
    DumpStats.save_defs(macros, "macros")

    UI.puts([:green, "Done!"])

    exec
  end
end
