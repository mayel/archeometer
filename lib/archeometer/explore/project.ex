defmodule Archeometer.Explore.Project do
  @moduledoc """
  Utilities for obtaining higher level information about a project, mostly
  through Mix and Application.
  """

  require Logger

  @doc """
  Get the (underscored) module name of the current project`mix.exs` file. Every
  directory with one is considered a project by Mix.
  """
  def get!() do
    Mix.Project.get!()
    |> Macro.to_string()
    |> String.split(".")
    |> hd()
    |> Macro.underscore()
  end

  @doc """
  Get all the locally defined modules.
  """
  def local_modules() do
    local_modules_by_app()
    |> Map.values()
    |> List.flatten()
  end

  @doc """
  Obtain all the locally defined modules by application.
  """
  def local_modules_by_app() do
    apps = apps()

    Enum.each(apps, fn app ->
      case Application.ensure_loaded(app) do
        {:error, reason} ->
          Logger.warn(
            "#{app} failed to load with #{inspect(reason)}; it will be ignored in further analysis"
          )

        ok ->
          ok
      end
    end)

    Map.new(apps, fn app -> {app, Application.spec(app, :modules)} end)
  end

  @doc """
  Obtain the list of local app in the current project. This is the value of
  `:app` in regular projects and all the children apps in umbrella projects.
  """
  def apps() do
    case Mix.Project.config()[:app] do
      nil ->
        umbrella_apps()

      app when is_atom(app) ->
        case Mix.Project.config()[:multirepo_deps] do
          apps when is_list(apps) -> [app] ++ dep_names(apps)
          _ -> [app]
        end
    end
  end

  @doc """
  Given a file path, try to guess its application. If the project is not
  umbrella, just return the project app. Else try to parse the path to guess the
  application.
  """
  def guess_app_from_path(file_path) do
    if umbrella_or_multirepo?() do
      case Regex.run(~r{[apps|deps|forks]/([^/]+)}, file_path) do
        [_, app_name] ->
          {:ok, String.to_atom(app_name)}

        _ ->
          {:error, :failed_to_guess}
      end
    else
      {:ok, Mix.Project.config()[:app]}
    end
  end

  @doc """
  Obtain all the apps defined as umbrella children.
  """
  def umbrella_apps() do
    all_children = Mix.Dep.Loader.children()

    for %Mix.Dep{scm: Mix.SCM.Path, app: app, opts: opts} <- all_children,
        opts[:from_umbrella] do
      app
    end
  end

  def umbrella_or_multirepo? do
    Mix.Project.umbrella?() or not is_nil(Mix.Project.config()[:multirepo_deps])
  end

  defp dep_names(deps) when is_list(deps), do: Enum.flat_map(deps, &dep_names/1)
  defp dep_names(dep) when is_tuple(dep), do: [elem(dep, 0)]
  defp dep_names(dep) when is_atom(dep), do: [dep]
end
