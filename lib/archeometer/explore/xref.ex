defmodule Archeometer.Explore.XRef do
  @moduledoc """
  Module for collecting cross references between modules defined in the same
  project.
  """

  def compile_xrefs() do
    full_deps = compile_full_refs()
    local_mods = Archeometer.Explore.Project.local_modules() |> MapSet.new()

    full_deps
    |> Enum.filter(&(&1.callee in local_mods))
  end

  defp compile_full_refs() do
    ets = :ets.new(__MODULE__, [:named_table, :duplicate_bag, :public])

    try do
      Mix.Task.clear()

      # optionally recompile some deps, using a function specified by your app (typically using a mix task like `mix deps.compile`)
      multirepo_recompile_fn = Mix.Project.config()[:multirepo_recompile_fn]
      if is_function(multirepo_recompile_fn), do: multirepo_recompile_fn.()

      Mix.Task.run("compile", ["--force", "--tracer", __MODULE__])

      :ets.tab2list(__MODULE__)
      |> Enum.map(fn {_caller, ref} -> ref end)
    after
      :ets.delete(ets)
    end
  end

  def trace({:require, _meta, module, _opts}, env),
    do: save_event({:require, module}, env)

  def trace({:struct_expansion, _meta, module, _keys}, env),
    do: save_event({:struct, module}, env)

  def trace({:remote_function, _meta, module, _function, _arity}, env),
    do: save_event({:remote_function, module}, env)

  def trace({:remote_macro, _meta, module, _function, _arity}, env),
    do: save_event({:remote_macro, module}, env)

  def trace({:imported_function, _meta, module, _function, _arity}, env),
    do: save_event({:imported_function, module}, env)

  def trace({:imported_macro, _meta, module, _function, _arity}, env),
    do: save_event({:imported_macro, module}, env)

  def trace(_event, _env),
    do: :ok

  def save_event({type, callee}, env) do
    dep = %{
      caller: env.module,
      callee: callee,
      type: type,
      file: env.file,
      line: env.line
    }

    :ets.insert(__MODULE__, {env.module, dep})

    :ok
  end
end
