defmodule Archeometer.Explore.Static do
  @moduledoc """
  Module for analysing AST and collecting some useful information For now the
  schemas are hardcoded. There are

  - Modules
  - Definitions (functions, macros)

  Hopefully in the future a more extendible solution will be in place.
  """

  def process_module(full_ast, mod) do
    %{
      name: Archeometer.Util.Code.resolve_mod_name(full_ast, mod),
      num_lines: Archeometer.Util.Code.num_lines(mod)
    }
  end

  def process_def(full_ast, df) do
    {type, _meta, _children} = df
    {name, _meta, args} = Archeometer.Util.Code.get_decl(df)

    %{
      name: name,
      type: type,
      num_lines: Archeometer.Util.Code.num_lines(df),
      cc: Credo.Check.Refactor.CyclomaticComplexity.complexity_for(df),
      args: args,
      module: Archeometer.Util.Code.resolve_mod_name(full_ast, df)
    }
  end

  def module_stats_for(source_files) when is_list(source_files),
    do: Enum.flat_map(source_files, &module_stats_for/1)

  def module_stats_for(source_file) do
    path = source_file.filename

    ast =
      source_file
      |> Credo.SourceFile.ast()

    ast
    |> Archeometer.Util.Code.collect_node(:defmodule)
    |> Enum.map(&Map.put(process_module(ast, &1), :path, path))
  end

  def def_stats_for(source_files, defs) when is_list(source_files),
    do: Enum.flat_map(source_files, &def_stats_for(&1, defs))

  def def_stats_for(source_file, defs) do
    ast =
      source_file
      |> Credo.SourceFile.ast()

    ast
    |> Archeometer.Util.Code.collect_nodes(defs)
    |> Enum.map(&process_def(ast, &1))
  end
end
