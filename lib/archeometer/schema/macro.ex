defmodule Archeometer.Schema.Macro do
  @moduledoc """
  Represents an Elixir macro. Holds the relevant schema data that will be
  used in the project analysis.

  This schema has the following fields:
  - `name` if the name of the macro.
  - `cc` if the cyclomatic complexity of the macro declaration.
  - `num_lines` is the length of macro declaration.
  - `num_args` is the number of arguments the macro receives.
  - `arg_names` is a comma separated string with the name of the parameters the
  macro receives.
  - `type` can be either `defmacro` or `defmacrop`, depending on the
  declaration.
  - `coverage` is a number between 0 and 1 representing the test coverage of the
  macro. The results are pulled from Erlang `code` module so they might not be
  very accurate.
  - `module` is a reference to `Archeometer.Schema.Module`. The module where the
  macro was declared.
  """

  use Archeometer.Schema

  alias Archeometer.Schema.Module

  defschema(:macros) do
    field(:id, primary_key: true)
    field(:name)
    field(:cc)
    field(:num_lines)
    field(:num_args)
    field(:arg_names)
    field(:type)
    field(:coverage)
    belongs_to(Module)
  end
end
