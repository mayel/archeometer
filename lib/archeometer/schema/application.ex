defmodule Archeometer.Schema.Application do
  @moduledoc """
  This modules represents a OTP application. It holds the relevant schema data
  that will be used in the project analysis.

  This schema has the following fields:
  - `name` is the name of the OTP application
  - `modules` is a reference to `Archeometer.Schema.Modules`. All the modules
  belonging to the application.
  """

  use Archeometer.Schema

  alias Archeometer.Schema.Module

  defschema :apps do
    field(:id, primary_key: true)
    field(:name)
    has(Module, as: :modules, key: :app_id)
  end
end
