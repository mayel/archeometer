defmodule Archeometer.Schema.Module do
  @moduledoc """
  Represents an Elixir module. Holds the relevant schema data that will be
  used in the project analysis.

  This schema has the following fields:
  - `name` is the name of the module.
  - `num_lines` is the length of the module delcaration body.
  - `coverage` is a number between 0 and 1 representing the module testing
  coverage percentage.
  - `path` is the file path where the module was declared.
  - `app` is a reference to `Archeometer.Schema.Application`. It is the module's
  corresponding OTP application
  - `in_refs` stands for "incoming references". That is, all references of the
  current module in other modules. Or in other words all the usages of the
  current module.
  - `out_refs` stands for "outgoing reference". That is, all references from other
  modules in the current one. Or said on other words, the current module
  dependencies.
  - `functions` is a reference to `Archeometer.Schema.Function`. All the
  functions defined in the module.
  - `macros` is a reference to `Archeometer.Schema.Macro`. All the macros
  defined in the module.
  """

  use Archeometer.Schema

  alias Archeometer.Schema.{Application, XRef, Function, Macro}

  defschema(:modules) do
    field(:id, primary_key: true)
    field(:name)
    field(:num_lines)
    field(:coverage)
    field(:path)
    belongs_to(Application, key: :app_id)
    has(XRef, as: :in_refs, key: :callee_id)
    has(XRef, as: :out_refs, key: :caller_id)
    has(Function, as: :functions)
    has(Macro, as: :macros)
  end
end
