defmodule Archeometer.Graph.GraphvizTest do
  use ExUnit.Case
  alias Archeometer.Graphs.Graphviz
  doctest Archeometer.Graphs.Graphviz, import: true

  @non_cyclic %{
    1 => [],
    2 => [1, 3],
    3 => [1],
    4 => [2, 3]
  }

  @cyclic %{
    1 => [4],
    2 => [1],
    3 => [1, 2],
    4 => [2]
  }

  describe "render dot" do
    test "with non cyclic reference" do
      dot_str = Graphviz.render_dot(@non_cyclic)

      assert String.starts_with?(dot_str, "digraph G {")
      assert String.ends_with?(dot_str, "}\n")

      lines = String.split(dot_str, "\n")

      # For modules

      Enum.each(Map.keys(@non_cyclic), fn module ->
        assert is_module_declared?(lines, module)
      end)

      # For relationships

      Enum.each(@non_cyclic, fn {origin, dests} ->
        Enum.each(dests, fn dest ->
          is_reference_declared?(lines, origin, dest)
        end)
      end)
    end

    test "with cyclic reference" do
      dot_str = Graphviz.render_dot(@cyclic)

      assert String.starts_with?(dot_str, "digraph G {")
      assert String.ends_with?(dot_str, "}\n")

      lines = String.split(dot_str, "\n")

      # For modules

      Enum.each(Map.keys(@non_cyclic), fn module ->
        assert is_module_declared?(lines, module)
      end)

      # For relationships

      Enum.each(@non_cyclic, fn {origin, dests} ->
        Enum.each(dests, fn dest ->
          is_reference_declared?(lines, origin, dest)
        end)
      end)
    end
  end

  describe "render png" do
    @tag :graphviz
    test "graphviz format: png, with non cyclic reference" do
      assert {:ok, {:image, :png}} =
               @non_cyclic
               |> Graphviz.render_image()
               |> MagicNumber.detect()
    end

    @tag :graphviz
    test "graphviz format: png, with cyclic reference" do
      assert {:ok, {:image, :png}} =
               @cyclic
               |> Graphviz.render_image()
               |> MagicNumber.detect()
    end
  end

  defp is_module_declared?(lines, module) do
    {:ok, rx} = Regex.compile("^[[:space:]]+\"#{module}\";")
    Enum.count(lines, fn line -> Regex.match?(rx, line) end) == 1
  end

  defp is_reference_declared?(lines, from, to) do
    {:ok, rx} = Regex.compile("[[:space:]]+\"#{from}\" -> \"#{to}\";")
    Enum.count(lines, fn line -> Regex.match?(rx, line) end) == 1
  end
end
