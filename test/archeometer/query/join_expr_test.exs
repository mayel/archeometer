defmodule Archeometer.Query.JoinExprTest do
  use ExUnit.Case
  use Archeometer.SchemaFixtures

  alias Archeometer.Query.{Term, Symbol, JoinExpr}

  defp initial_aliases_for(module),
    do: %{m: {:table_alias, [alias?: true, key?: true, dest: module]}}

  describe "expressions without symbols" do
    test "literals generate no joins" do
      {:__block__, _meta, asts} =
        quote do
          10.99
          "foo"
          true
          10
        end

      Enum.each(asts, fn ast ->
        assert joins = JoinExpr.generate_joins(ast)
        assert map_size(joins) == 0
      end)
    end

    test "operators with literals generate no joins" do
      {:__block__, _meta, asts} =
        quote do
          avg(35)
          like(false, "false")
          90 < 2
          "hola" == "foo"
        end

      Enum.each(asts, fn ast ->
        assert joins = JoinExpr.generate_joins(ast)
        assert map_size(joins) == 0
      end)
    end

    test "fragments generate no joins" do
      ast = quote do: ^"ARRAY_ACC(table)"

      assert joins = JoinExpr.generate_joins(ast)
      assert map_size(joins) == 0
    end
  end

  describe "symbols" do
    test "field generates no joins" do
      ast = quote do: m.id

      {:ok, validated} = Term.validate(ast)
      {:ok, resolved} = Symbol.resolve_symbols(validated, initial_aliases_for(ModuleA))

      assert joins = JoinExpr.generate_joins(resolved)
      assert map_size(joins) == 0
    end

    test "single key generates single join" do
      ast = quote do: m.module_b.id

      {:ok, validated} = Term.validate(ast)
      {:ok, resolved} = Symbol.resolve_symbols(validated, initial_aliases_for(ModuleA))

      assert %{"m.module_b" => %JoinExpr{}} =
               JoinExpr.generate_joins(
                 resolved,
                 ModuleA,
                 alias: :m
               )
    end

    test "nested keys generate more joins" do
      ast = quote do: m.module_b.module_a.id

      {:ok, validated} = Term.validate(ast)
      {:ok, resolved} = Symbol.resolve_symbols(validated, initial_aliases_for(ModuleC))

      assert %{"m.module_b" => %JoinExpr{}, "m.module_b.module_a" => %JoinExpr{}} =
               JoinExpr.generate_joins(resolved, ModuleC, alias: :m)
    end

    test "operator with single symbol generates joins" do
      ast = quote do: like(m.module_b.id, "%Module")

      {:ok, validated} = Term.validate(ast)
      {:ok, resolved} = Symbol.resolve_symbols(validated, initial_aliases_for(ModuleA))

      assert %{"m.module_b" => %JoinExpr{}} =
               JoinExpr.generate_joins(
                 resolved,
                 ModuleA,
                 alias: :m
               )
    end

    test "operator with multiple nested symbols generates joins" do
      ast = quote do: like(m.module_b.id, m.module_b.module_a.id)

      {:ok, validated} = Term.validate(ast)
      {:ok, resolved} = Symbol.resolve_symbols(validated, initial_aliases_for(ModuleA))

      assert %{"m.module_b" => %JoinExpr{}, "m.module_b.module_a" => %JoinExpr{}} =
               JoinExpr.generate_joins(resolved, ModuleA, alias: :m)
    end
  end

  describe "alias synthesis" do
    @test_alias_tables [
      empty: %{},
      non_empty: %{"0" => %JoinExpr{}},
      non_empty: %{"0" => %JoinExpr{}, "1" => %JoinExpr{}}
    ]

    for {kind, tables} <- @test_alias_tables do
      test "#{kind} table `#{inspect(tables)}` returns valid alias" do
        t = unquote(Macro.escape(tables))
        assert "u" <> i = JoinExpr.next_alias_id(t)

        other = Map.put(t, "other", %JoinExpr{})
        assert "u" <> j = JoinExpr.next_alias_id(other)

        assert i != j
      end
    end
  end

  describe "serialization" do
    @test_join %{
      join: %JoinExpr{
        source: %JoinExpr.Table{module: ModuleA, alias: "u0", key: :id},
        dest: %JoinExpr.Table{module: ModuleB, alias: "u1", key: :id}
      },
      serialized: %JoinExpr{
        source: %JoinExpr.Table{module: "module_a", alias: "u0", key: "id"},
        dest: %JoinExpr.Table{module: "module_b", alias: "u1", key: "id"}
      }
    }

    test "for tables works" do
      original = @test_join.join.source
      serialized = @test_join.serialized.source
      assert serialized == JoinExpr.Table.serialize(original)
    end

    test "for joins works" do
      assert @test_join.serialized == JoinExpr.serialize(@test_join.join)
    end
  end
end
