defmodule Archeometer.Query.BuilderTest do
  use ExUnit.Case
  use Archeometer.SchemaFixtures

  alias Archeometer.Query
  alias Archeometer.Query.Builder

  defp build_query_with_field(field, ast) do
    query = Query.initial_query_for(ModuleA, alias: :m)
    Builder.build({:ok, query}, [{field, ast}])
  end

  defp build_query_with_repeated_field(field, ast) do
    query = Query.initial_query_for(ModuleA, alias: :m)
    Builder.build({:ok, query}, [{field, ast}, {field, ast}])
  end

  test "with no options works" do
    {:ok, _} = Builder.build({:ok, Query.initial_query_for(ModuleA, alias: :m)}, [])
  end

  test "non query fields fails" do
    {:error, error} =
      Builder.build(
        {:ok, Query.initial_query_for(ModuleA, alias: :m)},
        [{:not_here, quote(do: id)}]
      )

    assert String.contains?(error, "invalid")
    assert String.contains?(error, "field")
  end

  test "error propagates" do
    assert {:error, :noop} = Builder.build({:error, :noop}, [])
  end

  Module.put_attribute(
    __MODULE__,
    :valid_mod_a_selections,
    quote do
      [
        field: m.id,
        key: m.module_b.id,
        nested_key: m.module_b.module_c.id,
        tuple: {m.id, m.module_b.id, m.module_b.module_c.id},
        list: [m.id, m.module_b.id, m.module_b.module_c.id],
        map: %{id: m.id, mod_b: m.module_b.id, mod_c: m.module_b.module_c.id},
        keyword_list: [id: m.id, mod_b: m.module_b.id, mod_c: m.module_b.module_c.id]
      ]
    end
  )

  @full_expression_keywords [:select, :group_by]

  describe "container expressions" do
    for field <- @full_expression_keywords do
      for {type, ast} <- @valid_mod_a_selections do
        test "in #{field} with correct #{type} `#{Macro.to_string(ast)}` works" do
          assert {:ok, q} =
                   build_query_with_field(
                     unquote(Macro.escape(field)),
                     unquote(Macro.escape(ast))
                   )

          assert [_ | _] = Map.get(q, unquote(Macro.escape(field)))
        end

        test "with reapted #{field} with correct #{type} `#{Macro.to_string(ast)}` works" do
          assert {:ok, q} =
                   build_query_with_repeated_field(
                     unquote(Macro.escape(field)),
                     unquote(Macro.escape(ast))
                   )

          assert [expr, expr | _] = Map.get(q, unquote(Macro.escape(field)))
        end
      end
    end

    Module.put_attribute(
      __MODULE__,
      :invalid_mod_a_selections,
      quote do
        [
          field: m.not_here,
          tuple: {m.id, m.not_here},
          tuple: {m.not_here, m.id},
          list: [m.not_here, m.id],
          map: %{id: m.id, m: m.not_here},
          map: %{m: m.not_here, id: m.id},
          keyword_list: [m: m.not_here, id: m.id]
        ]
      end
    )

    for {type, ast} <- @invalid_mod_a_selections do
      for field <- @full_expression_keywords do
        test "in #{field} with incorrect #{type} `#{Macro.to_string(ast)}` fails" do
          assert {:error, error} =
                   build_query_with_field(
                     unquote(Macro.escape(field)),
                     unquote(Macro.escape(ast))
                   )

          assert String.contains?(error, "resolve")
          assert String.contains?(error, "field")
        end
      end
    end
  end

  Module.put_attribute(
    __MODULE__,
    :valid_mod_a_comparaisons,
    quote do
      [
        comparaison: m.id <= m.module_b.id,
        equality: m.module_b.id == m.id,
        builtin_func: like(m.module_b.module_c.id, "%")
      ]
    end
  )

  @simple_expression_keyword [:where, :having]

  describe "simple expressions" do
    for field <- @simple_expression_keyword do
      for {type, ast} <- @valid_mod_a_comparaisons do
        test "in #{field} with correct #{type} `#{Macro.to_string(ast)}` works" do
          assert {:ok, _q} =
                   build_query_with_field(
                     unquote(Macro.escape(field)),
                     unquote(Macro.escape(ast))
                   )
        end
      end
    end
  end

  @valid_mod_a_limits [pos_integer: 10]
  @valid_distinct [bool: true, bool: false]

  Module.put_attribute(
    __MODULE__,
    :invalid_literals,
    quote do
      [
        field: m.id,
        tuple: {m.id, m.id},
        map: %{name: m.id},
        literal: :atom,
        literal: 1.0
      ]
    end
  )

  @literal_expression_keywords [:limit, :distinct]

  describe "literal expressions" do
    for {type, val} <- @valid_mod_a_limits do
      test "for limit with correct #{type} `#{val}`" do
        {:ok, q} = build_query_with_field(:limit, unquote(val))
        assert unquote(val) == Map.get(q, :limit)
      end
    end

    for {type, val} <- @valid_distinct do
      test " for distinct with correct #{type} `#{val}`" do
        {:ok, q} = build_query_with_field(:distinct, unquote(val))
        assert unquote(val) == Map.get(q, :distinct)
      end
    end

    for keyword <- @literal_expression_keywords do
      for {type, ast} <- @invalid_literals do
        @keyword keyword
        test "for #{@keyword} with incorrect #{type} `#{Macro.to_string(ast)}`" do
          assert {:error, _} =
                   build_query_with_field(
                     @keyword,
                     unquote(Macro.escape(ast))
                   )
        end
      end
    end
  end

  Module.put_attribute(
    __MODULE__,
    :valid_mod_a_order,
    quote do
      [
        field: m.id,
        key: m.module_b.id,
        nested_key: m.module_b.module_c.id,
        tuple: {m.id, m.module_b.id, m.module_b.module_c.id},
        list: [m.id, m.module_b.id, m.module_b.module_c.id],
        map: %{asc: m.id, desc: m.module_b.id, desc: m.module_b.module_c.id},
        keyword_list: [asc: m.id, desc: m.module_b.id, desc: m.module_b.module_c.id]
      ]
    end
  )

  Module.put_attribute(
    __MODULE__,
    :invalid_mod_a_order,
    quote do
      [
        map: %{asc: m.id, desc: m.not_here},
        map: %{name: m.id, mod: m.module_b.id},
        map: %{asc: m.id, mod: m.module_b.id}
      ]
    end
  )

  describe "order_by" do
    for {type, ast} <- @valid_mod_a_order do
      test "with correct #{type} `#{Macro.to_string(ast)}` works" do
        assert {:ok, q} = build_query_with_field(:order_by, unquote(Macro.escape(ast)))
        assert [_ | _] = Map.get(q, :order_by)
      end
    end

    for {type, ast} <- @invalid_mod_a_order do
      test "with incorrect #{type} `#{Macro.to_string(ast)}` fails" do
        assert {:error, _} = build_query_with_field(:order_by, unquote(Macro.escape(ast)))
      end
    end
  end
end
