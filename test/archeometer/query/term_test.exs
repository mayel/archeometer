defmodule Archeometer.Query.TermTest do
  use ExUnit.Case

  alias Archeometer.Query.Term

  describe "term validation" do
    @valid_literals [
      integer: 1,
      float: 1.0,
      bool: true,
      bool: false,
      string: "foo"
    ]

    for {type, lit} <- @valid_literals do
      test "#{type} literal `#{lit}` is valid" do
        assert {:ok, unquote(lit)} = Term.validate(unquote(lit))
      end
    end

    @invalid_literals [
      atom: :foo,
      atom: Foo,
      charlist: 'foo'
    ]

    for {type, lit} <- @invalid_literals do
      test "#{type} literal `#{lit}` is invalid" do
        assert {:error, _} = Term.validate(unquote(lit))
      end
    end

    Module.put_attribute(
      __MODULE__,
      :valid_escapes,
      quote do
        [
          escape: ^0,
          escape: ^10
        ]
      end
    )

    for {kind, ast} <- @valid_escapes do
      test "#{kind} fragment `#{Macro.to_string(ast)}` is valid" do
        assert {:ok, {:^, [], _}} = Term.validate(unquote(Macro.escape(ast)))
      end
    end

    Module.put_attribute(
      __MODULE__,
      :valid_builtins,
      quote do
        [
          prefix_unary: is_nil(1),
          prefix_unary: not is_nil(1),
          prefix_unary: avg(not is_nil(1)),
          prefix_unary: count(avg(not is_nil(1))),
          prefix_unary: max(count(avg(not is_nil(1)))),
          prefix_unary: min(max(count(avg(not is_nil(1))))),
          prefix_unary: sum(min(max(count(avg(not is_nil(1)))))),
          prefix_binary: like(1, 2),
          infix_binary: sum(1) * min(2),
          infix_binary: 2 * max(5) / max(3 * 3),
          infix_binary: 5 / 4 * avg(6) + avg(4 * 3),
          infix_binary: 4 + 5 * min(4) / 7 - (4 + 9),
          infix_binary: 1 == 0 - 0,
          infix_binary: avg(7) * 4 != 3,
          infix_binary: min(2) < 1 - 8 * count(0),
          infix_binary: 8 + 5 - 2 > max(4) / 5,
          infix_binary: sum(0) <= avg(4),
          infix_binary: is_nil(5) >= count(6),
          infix_binary: is_nil(5) >= count(6) and 8 + 5 - 2 > max(4) / 5,
          infix_binary: sum(0) <= avg(4) or is_nil(5) >= count(6)
        ]
      end
    )

    for {kind, ast} <- @valid_builtins do
      test "#{kind} builtin `#{Macro.to_string(ast)}` is valid" do
        assert {:ok, {_, [], [_ | _]}} = Term.validate(unquote(Macro.escape(ast)))
      end
    end

    Module.put_attribute(
      __MODULE__,
      :invalid_builtins,
      quote do
        [
          wrong_num_args: like,
          wrong_num_args: avg(),
          wrong_num_args: like(1),
          wrong_num_args: count(1, 2),
          wrong_num_args: max(1, 2, 3),
          wrong_num_args: min(1, 2, 3, 4),
          wrong_num_args: sum(1, 2, 3, 4, 5)
        ]
      end
    )

    for {kind, ast} <- @invalid_builtins do
      test "builint with #{kind} `#{Macro.to_string(ast)}` is invalid" do
        assert {:error, _} = Term.validate(unquote(Macro.escape(ast)))
      end
    end

    Module.put_attribute(
      __MODULE__,
      :valid_symbols,
      quote do
        [
          lookup: modules,
          lookup: macros.num_lines,
          lookup: modules.functions.cc,
          lookup: function.module,
          expr: modules.lines + functions.cc,
          expr: like(modules.name, "Elixir.%"),
          expr: is_nil(module.app),
          expr: not (functions.cc > 10),
          expr: functions.cc > 10,
          expr: sum(macros.cc) / count(macros.id),
          expr: max(functions.num_args) != min(macros.num_args),
          expr: max(num_lines) != min(macros.num_args) and functions.cc > 10,
          expr: max(num_lines) <= avg(functions.num_args) or macros.cc > 5
        ]
      end
    )

    for {kind, ast} <- @valid_symbols do
      test "symbol in #{kind} `#{Macro.to_string(ast)}` is valid" do
        assert {:ok, _} = Term.validate(unquote(Macro.escape(ast)))
      end
    end
  end

  describe "flatten" do
    Module.put_attribute(
      __MODULE__,
      :flat_symbols,
      quote do
        [
          simple: {foo, [:foo]},
          nested: {module.name, [:module, :name]},
          nested: {
            function.module.application.name,
            [:function, :module, :application, :name]
          }
        ]
      end
    )

    for {kind, {symb, expected}} <- @flat_symbols do
      test "#{kind} symbol `#{Macro.to_string(symb)}` directly gives #{inspect(expected)}" do
        assert {:ok, {:symb, [], flatten}} = Term.flatten_lookups(unquote(Macro.escape(symb)))
        assert unquote(expected) == flatten
      end

      test "#{kind} symbol `#{Macro.to_string(symb)}` after validation gives #{inspect({expected})}" do
        {:ok, ast} = Term.validate(unquote(Macro.escape(symb)))
        assert {:ok, {:symb, [], flatten}} = Term.flatten_lookups(ast)
        assert unquote(expected) = flatten
      end
    end

    Module.put_attribute(
      __MODULE__,
      :flat_expressions,
      quote do
        [
          expr: not module,
          expr: like(module.functions.name, "%"),
          expr: sum(module.cc) / count(module.functions)
        ]
      end
    )

    for {kind, ast} <- @flat_expressions do
      test "#{kind} symbol `#{Macro.to_string(ast)}` directly is ok" do
        assert {:ok, _} = Term.flatten_lookups(unquote(Macro.escape(ast)))
      end

      test "#{kind} expression `#{Macro.to_string(ast)}` after validation is ok" do
        {:ok, valid_ast} = Term.validate(unquote(Macro.escape(ast)))
        assert {:ok, _} = Term.flatten_lookups(valid_ast)
      end
    end

    Module.put_attribute(
      __MODULE__,
      :invalid_flat_expressions,
      quote do
        [
          expr: like(module.function, '%'),
          expr: module.cc + get(module.application),
          expr: avg(Foo, module.action)
        ]
      end
    )

    for {kind, ast} <- @invalid_flat_expressions do
      test "wrong #{kind} `#{Macro.to_string(ast)}` propagates errors" do
        assert {:error, _} = Term.flatten_lookups(unquote(Macro.escape(ast)))
      end
    end
  end

  describe "term serialization" do
    Module.put_attribute(
      __MODULE__,
      :serialized_terms,
      quote do
        [
          integer: {1, "1"},
          float: {1.0, "1.0"},
          bool: {true, "true"},
          bool: {false, "false"},
          string: {"foo", "'foo'"},
          prefix_binary: {like(1, 2), "(1 LIKE 2)"},
          infix_binary: {4 + 5 * min(4) / 7 - (4 + 9), "((4 + ((5 * min(4)) / 7)) - (4 + 9))"},
          infix_binary: {1 == 0 - 0, "(1 == (0 - 0))"},
          infix_binary: {not is_nil(5) >= count(6), "(5 NOTNULL >= count(6))"},
          infix_binary: {like(4, 5) or is_nil(5), "((4 LIKE 5) OR 5 ISNULL)"},
          expr: {modules.lines + functions.cc, "(modules.lines + functions.cc)"},
          expr: {like(modules.name, "Elixir.%"), "(modules.name LIKE 'Elixir.%\')"},
          expr: {is_nil(module.app), "module.app ISNULL"},
          expr: {not (functions.cc > 10), "NOT (functions.cc > 10)"}
        ]
      end
    )

    for {key, {ast, expected}} <- @serialized_terms do
      test "#{key} `#{Macro.to_string(ast)}` maps to `#{expected}`" do
        {:ok, valid} = Term.validate(unquote(Macro.escape(ast)))
        {:ok, flat} = Term.flatten_lookups(valid)
        assert {:ok, io_lit} = Term.to_iodata(flat)
        assert unquote(expected) == IO.iodata_to_binary(io_lit)
      end
    end

    Module.put_attribute(
      __MODULE__,
      :invalid_serialized_terms,
      quote do
        [
          expr: 5 * not_here(true, "foo"),
          expr: true and avg(not_here(), false),
          expr: avg(not_here(1), false)
        ]
      end
    )

    for {key, ast} <- @invalid_serialized_terms do
      test "#{key} `#{Macro.to_string(ast)}` fails" do
        assert {:error, error} = Term.to_iodata(unquote(Macro.escape(ast)))
        assert {:invalid_operator, _} = error
      end
    end
  end

  describe "term to ast" do
    defp assert_equiv({op0, _meta, module0}, {op1, _meta1, module1})
         when is_atom(module0) and is_atom(module1) do
      assert op0 == op1
    end

    defp assert_equiv({op0, _meta0, args0}, {op1, _meta1, args1}) do
      assert_equiv(op0, op1)

      Enum.zip(args0, args1)
      |> Enum.each(fn {arg0, arg1} ->
        assert_equiv(arg0, arg1)
      end)
    end

    defp assert_equiv(ast0, ast1), do: assert(ast0 == ast1)

    Module.put_attribute(
      __MODULE__,
      :valid_symbols,
      quote do
        [
          escaped: ^42,
          lookup: modules,
          lookup: macros.num_lines,
          lookup: modules.functions.cc,
          lookup: function.module,
          expr: modules.lines + functions.cc,
          expr: like(modules.name, "Elixir.%"),
          expr: is_nil(module.app) and true,
          expr: not (functions.cc > 10),
          expr: functions.cc > 5.09,
          expr: sum(macros.cc) / count(macros.id),
          expr: max(functions.num_args) != min(macros.num_args),
          expr: max(num_lines) != min(macros.num_args) and functions.cc > 10,
          expr: max(num_lines) <= avg(functions.num_args) or macros.cc > 5
        ]
      end
    )

    for {kind, ast} <- @valid_symbols do
      test "#{kind} `#{Macro.to_string(ast)}` validation can be reverted" do
        a = unquote(Macro.escape(ast))
        new_ast = Term.validate(a) |> elem(1) |> Term.to_ast()
        assert_equiv(new_ast, a)
      end
    end
  end
end
