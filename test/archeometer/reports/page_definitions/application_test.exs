defmodule Archeometer.Reports.PageDefinitions.ApplicationTest do
  use ExUnit.Case

  alias Archeometer.Reports.Page
  alias Archeometer.Reports.PageDefinition.Application

  @app_name "jissai"
  @test_db "test/resources/db/archeometer_jissai.db"

  @tag :graphviz
  test "page for app" do
    page_def = Application.definition(@app_name)
    %Page{sections: sections} = Page.process(page_def, [app: @app_name], @test_db)
    fragments = Enum.flat_map(sections, fn s -> s.fragments end)

    img_fragments = Enum.filter(fragments, fn f -> f.result_type == :image end)
    assert length(img_fragments) == 2

    Enum.each(img_fragments, fn f ->
      ext = String.split(f.result, ".") |> List.last()
      assert valid_img?(f.result, ext)
    end)
  end

  defp valid_img?(path, ext) do
    File.regular?(path) and String.ends_with?(path, ext)
  end
end
