defmodule Archeometer.Reports.PageDefinitions.ProjectTest do
  use ExUnit.Case

  alias Archeometer.Reports.Page
  alias Archeometer.Reports.Fragment
  alias Archeometer.Reports.PageDefinition.Project
  alias Archeometer.Reports.FragmentDefs

  @project_name "jissai"
  @test_db "test/resources/db/archeometer_jissai.db"

  test "page for project" do
    page_def = Project.definition(@project_name)

    %Page{sections: sections} = Page.process(page_def, [])

    assert [f1, f2] = Enum.flat_map(sections, fn s -> s.fragments end)

    assert f1.result_type == :table
    assert f2.result_type == :image

    assert %{
             headers: ["name", "num_mods", "num_lines"],
             values: [
               ["jissai_web", 61, 3290],
               ["jissai", 40, 6382]
             ]
           } = f1.result
  end

  test "Validate CQl - SQL - List of applications and their module count" do
    fragment = FragmentDefs.Application.applications_and_modules()
    alt_code_res = Fragment.alt_process(fragment, [app: "jissai"], @test_db) |> Map.get(:result)
    cql_res = Fragment.process(fragment, app: "jissai") |> Map.get(:result)
    assert alt_code_res == cql_res
  end
end
