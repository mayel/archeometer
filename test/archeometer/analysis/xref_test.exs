defmodule Archeometer.Analysis.XrefTest do
  use ExUnit.Case

  alias Archeometer.Analysis.Xref

  @test_db "test/resources/db/archeometer_jissai.db"
  @modules ["Jissai.AtamaData", "Jissai.AtamaRepo"]

  test "Xref.gen_graph with dot format" do
    assert Xref.gen_graph(@modules, "dot", @test_db) ==
             """
             digraph G {
                 "Jissai.AtamaData";
                 "Jissai.AtamaRepo";
                   "Jissai.AtamaData" -> "Jissai.AtamaRepo";
             }
             """
  end

  test "Xref.gen_graph with the default argument for database" do
    assert Xref.gen_graph(@modules, "dot") ==
             """
             digraph G {
                 "Jissai.AtamaData";
                 "Jissai.AtamaRepo";
                   "Jissai.AtamaData" -> "Jissai.AtamaRepo";
             }
             """
  end

  @tag :graphviz
  test "Xref.gen_graph with png format" do
    assert {:ok, {:image, :png}} ==
             @modules
             |> Xref.gen_graph("png", @test_db)
             |> MagicNumber.detect()
  end

  test "Xref.gen_graph with mermaid format" do
    assert Xref.gen_graph(@modules, "mermaid", @test_db) ==
             """
             graph TD;
               id_0([Jissai.AtamaData]);
               id_1([Jissai.AtamaRepo]);
               id_0-->id_1;
             """
  end

  test "Xref.gen_graph with unsupported format" do
    assert Xref.gen_graph(@modules, "Wrong.Format", @test_db) == {:error, :unsupported_format}
  end

  test "Xref.gen_graph with non existing database" do
    assert Xref.gen_graph(@modules, "dot", "Non.Existing.DB") ==
             {:error, "no such table: modules"}

    System.cmd("rm", ["Non.Existing.DB"])
  end

  test "Xref.gen_graph with a non existing module" do
    assert Xref.gen_graph(["Non.Existent.Module"], "dot", @test_db) ==
             {:error, "unknown module 'Non.Existent.Module'"}
  end
end
