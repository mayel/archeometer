defmodule Archeometer.Analysis.Treemap.DataTest do
  use ExUnit.Case

  alias Archeometer.Analysis.Treemap.Node
  alias Archeometer.Analysis.Treemap
  alias Archeometer.Analysis.Treemap.SVGRender

  @db_path "./test/resources/db/archeometer_jissai.db"
  @jissai_modules 24
  @jissai_test_modules 13
  @jissai_web_modules 51

  describe "create treemap from db" do
    test "all modules within an application, including tests" do
      tree = Treemap.treemap(:size, :jissai, "*", @db_path, false)
      file = SVGRender.render(tree)
      lines = String.split(file, "\n")

      assert count_modules(lines, "Jissai") == @jissai_modules + @jissai_test_modules
    end

    test "all non test related modules within an application" do
      tree = Treemap.treemap(:size, :jissai, "*", @db_path, true)
      file = SVGRender.render(tree)
      lines = String.split(file, "\n")

      assert count_modules(lines, "Jissai") == @jissai_modules
    end

    test "modules without specifying the app name" do
      tree = Treemap.treemap(:size, :none, "*", @db_path, true)
      file = SVGRender.render(tree)
      lines = String.split(file, "\n")

      assert count_modules(lines, "Jissai") == @jissai_modules
      assert count_modules(lines, "JissaiWeb") == @jissai_web_modules
    end

    test "a module only by it's name" do
      tree = Treemap.treemap(:size, :none, "Jissai.Application", @db_path, true)

      assert [
               %Node{
                 name: "Jissai.Application",
                 pct: 100.0
               }
             ] = tree.nodes
    end

    test "an especific module from a particular app" do
      tree =
        Treemap.treemap(
          :size,
          :jissai,
          "Jissai.Application",
          @db_path,
          true
        )

      assert [
               %Node{
                 name: "Jissai.Application",
                 pct: 100.0
               }
             ] = tree.nodes
    end
  end

  defp count_modules(lines, app) do
    {:ok, rx} = Regex.compile("\s+<title>#{app}\\.[A-Z].+</title>")
    Enum.count(lines, fn line -> Regex.match?(rx, line) end)
  end
end
