alias Archeometer.Reports

Reports.Config.report_path(:html)
|> File.mkdir_p!()

Reports.Config.static_report_img_path()
|> File.mkdir_p!()

ExUnit.start()
