# Basic Usage

Since this is a tool about project discovery, first we have to collectall the data.

## Collect the data
Data collection is divided in several phases, depending on the requirements of each phase. To collect all the data simply run
```sh
mix arch.explore
```

This will run all sorts of different analysis and store all the data in a `archeometer_<your_project>.db` file.

## Collection stages
You can also run each phase individually. They are
+ `arch.explore.static`: analysis the AST of the project code. It requires [Credo](https://github.com/rrrene/credo).
+ `arch.explore.xrefs`: collects information about module dependencies. It forces a compilation.
+ `arch.explore.apps`: get information about the applications in the project. It must compile the project.
+ `arch.explore.coverage`: calculate test coverage per module and function. It must run the test suite.

Depending on your project, you can choose to run only the stage you are interested, or to skip some stage that is specially expensive to run.

## Creating reports
There are some predefined tasks to give you a brief overview of the project.

+ `mix arch.dsm`: will create a Design Structure Matrix, and will use it to find the cyclic dependencies in the project.
+ `mix arch.xref` : will create a dependency graph in several possible formats.
+ `mix arch.report.html` : Creates a comprehensive report on the project under review.

## Further reading
If you want to have a more in depth exploration of your project, don't forget to read the following guides:
+[Queries and Schemas](../cql/queries_and_schemas.md): make queries to the data base using Elixir code
