# Installation

This is a Mix package, so you must install it [using Mix](http://elixir-lang.org/getting-started/mix-otp/introduction-to-mix.html). For the time being, this package is not in Hex. To use it you can specify the Git repository directly in your project `mix.exs`:
```elixir
def deps do
  [
    {:archeometer, git: "https://gitlab.bunsan.io/RD/archeometer", only: [:dev, :test], runtime: false},
  ]
end
```

Or alternatively, you can clone this repository and add the dependency as a  `path: "path/to/the/repo"` instead.

And the just run
```sh
mix deps.get
```
