defmodule Archeometer.MixProject do
  use Mix.Project

  def project do
    [
      app: :archeometer,
      version: "0.1.0",
      elixir: "~> 1.10",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ],

      # documentation information
      name: "Archeometer",
      description: "Tool to explore the architecture, design and quality of your elixir projects",
      package: [
        name: "archeometer",
        licenses: ["MIT"],
        links: %{"Gitlab" => "https://gitlab.com/MachinesAreUs/archeometer"}
      ],
      docs: docs()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :tools]
    ]
  end

  defp docs() do
    [
      main: "overview",
      extras: extras(),
      extra_section: "GUIDES",
      groups_for_extras: groups_for_extras(),
      groups_for_modules: groups_for_modules(),
      nest_modules_by_prefix: nest_modules_by_prefix()
    ]
  end

  defp extras() do
    [
      "guides/introduction/overview.md",
      "guides/introduction/installation.md",
      "guides/introduction/basic_usage.md",
      "guides/cql/queries_and_schemas.md"
    ]
  end

  defp groups_for_extras() do
    [
      Introduction: Path.wildcard("guides/introduction/*.md"),
      "Code Query Language": Path.wildcard("guides/cql/*.md")
    ]
  end

  defp groups_for_modules() do
    [
      "Project Exploration": ~r{^Archeometer.Explore},
      "Report generation": ~r{^Archeometer.Reports},
      Analysis: ~r{^Archeometer.Analysis},
      Visualization: ~r{^Archeometer.Graphs},
      "Query Language": ~r{^Archeometer.Query},
      "Project Schema": ~r{^Archeometer.Schema},
      "Executing Queries": ~r{^Archeometer.Repo},
      Utilities: ~r{^Archeometer.Util}
    ]
  end

  # Modules with names too long to show in the side bar
  defp nest_modules_by_prefix() do
    [
      Archeometer.Analysis.DSM,
      Archeometer.Graphs,
      Archeometer.Query.Term,
      Archeometer.Reports,
      Archeometer.Schema,
      Archeometer.Explore
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test"]
  defp elixirc_paths(_), do: ["lib"]

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:temp, "~> 0.4"},
      {:credo, "~> 1.6.6"},
      {:exqlite, "~> 0.11.1"},
      {:uuid, "~> 1.1"},
      {:excoveralls, "~> 0.10", only: :test},
      {:slime, "~> 1.3"},
      {:ex_doc, "~> 0.27.0", only: :dev, runtime: false},
      {:magic_number, "~> 0.0.4", only: :test},
      {:css_colors, "~> 0.2.2"}
    ]
  end
end
